import 'dart:async';

class MeuBloc {
  int _total = 0;
  double sizeInicial = 100.0;

  final _blocController = StreamController<int>();
  final _blocController2 = StreamController<double>();

  Stream<int> get numumeroAtual => _blocController.stream;
  Stream<double> get numCrescer => _blocController2.stream;

  void incrementar() {
    _total++;
    _blocController.sink.add(_total);
  }

  void crescer() {
    sizeInicial += 10.0;
    _blocController2.sink.add(sizeInicial);
  }

  void diminuir() {
    sizeInicial -= 10.0;
    _blocController2.sink.add(sizeInicial);
  }

  void resetarContainer() {

    sizeInicial = 100.0;
    _blocController2.sink.add(sizeInicial);
  }

  void fecharStream2() {
    _blocController2.close();
  }

  void fecharStream() {
    _blocController.close();
  }
}
