import 'package:bloc_aula3/bloc.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  MeuBloc bloc = MeuBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter para Iniciantes - BLoC'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            StreamBuilder<int>(
                stream: bloc.numumeroAtual,
                initialData: 0,
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return Text('Há um erro na Stream');
                  } else {
                    return Text(
                      '${snapshot.data}',
                      style: TextStyle(fontSize: 40),
                    );
                  }
                }),
            StreamBuilder<double>(
              stream: bloc.numCrescer,
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return Text("ouve um erro");
                } else {
                  return InkWell(
                    onTap: bloc.crescer,
                    onDoubleTap: bloc.diminuir,
                    onLongPress: bloc.resetarContainer,
                    child: Container(
                      //child: Text("${snapshot.data}"),
                      color: Colors.green,
                      height: bloc.sizeInicial,
                      width: bloc.sizeInicial,
                    ),
                  );
                }
              },
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: bloc.incrementar,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }

  @override
  void dispose() {
    bloc.fecharStream();
    super.dispose();
  }
}
